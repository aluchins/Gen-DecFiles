# EventType: 34312001
#
# Descriptor: (K_S0 -> e+ mu-) (K_S0 -> e- mu+)
#
# NickName: KS_emu=TightCut,rho
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: (K_S0 -> e mu) with phase space model and tight generator cut
#  * KS0 endvertex z in [-1m,1m]
#  * KS0 endvertex radial cut at 30mm
# EndDocumentation
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[KS0 -> e+ mu-]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV" ,
#     "GVX = LoKi.GenVertices.PositionX() " ,
#     "GVY = LoKi.GenVertices.PositionY() " ,
#     "GVZ = LoKi.GenVertices.PositionZ() " ,
#     "vx    = monitor( GFAEVX ( GVX, 100 * meter )  , ' vx-Ks\n')  " ,    
#     "vy    = monitor( GFAEVX ( GVY, 100 * meter )  , ' vy-Ks\n')  " ,
#     "rho2  = monitor(          vx**2 + vy**2       , ' rho2-Ks\n')" ,
#     "rhoK  = monitor( rho2 < (30 * millimeter )**2 , ' rhoCut\n') " , 
#     "decay = monitor( in_range ( -1 * meter, monitor( GFAEVX ( GVZ, 100 * meter ), ' SVZ-Ks\n'), 1 * meter ), ' SVZCut\n') ",
# ]
# tightCut.Cuts      =    {
#     'KS0'  : ' decay & rhoK',
#                         }
# EndInsertPythonCode
#
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Miguel Ramos Pernas
# Email: Miguel.Ramos.Pernas@cern.ch
# Date: 20170213
#
Decay K_S0sig
  0.5     e+ mu-      PHSP;
  0.5     e- mu+      PHSP;
Enddecay
#
End
